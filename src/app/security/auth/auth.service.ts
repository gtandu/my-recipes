import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { Router } from "@angular/router";
import * as auth from 'firebase/auth';
import firebase from "firebase/compat";
import { BehaviorSubject } from "rxjs";
import UserCredential = firebase.auth.UserCredential;

export class LoginData {
  constructor(public email: string, public password: string) {
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public currentUserLogin: string | null | undefined;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router) {

    afAuth.authState.subscribe((authState) => {
      if (authState) {
        this.currentUserLogin = authState.email;
        this.isLoggedIn.next(true);
      }
    });
  }

  login(loginData: LoginData) {
    this.afAuth.signInWithEmailAndPassword(loginData.email, loginData.password)
      .then(this.getOnfulfilled())
      .catch(this.getOnrejected());
  }

  emailSignup(email: string, password: string) {
    this.afAuth.createUserWithEmailAndPassword(email, password)
      .then(this.getOnfulfilled())
      .catch(this.getOnrejected());
  }

  private getOnrejected() {
    return (error: any) => {
      console.log('Something went wrong: ', error);
    };
  }

  private getOnfulfilled() {
    return (value: UserCredential) => {
      console.log('Sucess', value);
      this.router.navigateByUrl('/home');
      this.isLoggedIn.next(true);
    };
  }

  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.oAuthLogin(provider)
      .then(this.getOnfulfilled())
      .catch(this.getOnrejected());
  }

  logout() {
    this.afAuth.signOut().then(() => {
      this.isLoggedIn.next(false);
      this.router.navigate(['/auth']);
    });
  }

  private oAuthLogin(provider: any) {
    return this.afAuth.signInWithPopup(provider);
  }
}
