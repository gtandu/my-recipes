import { Component, OnInit } from '@angular/core';
import { RecipeService } from "../../services/recipe.service";
import { Recipe } from "../../models/recipe";
import { AuthService } from "../../security/auth/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public recipes: Recipe[] = [];

  constructor(private recipeService: RecipeService, private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.recipeService.getAllByUser(this.authService.currentUserLogin).subscribe((recipes) => {
      console.log(recipes);
      this.recipes = recipes;
    });
  }

  public showRecipeDetails(id: string){
    this.router.navigate(['/recipe', id])
  }

}
