import { NgModule } from '@angular/core';
import { HomePageComponent } from "./home-page/home-page.component";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { SharedPrimeNgModule } from "../shared/shared-prime-ng/shared-prime-ng.module";

const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomePageComponent }
];

@NgModule({
  declarations: [HomePageComponent],
  imports: [SharedModule, RouterModule.forChild(routes), SharedPrimeNgModule],
  exports: [RouterModule]
})
export class HomeModule {
}
