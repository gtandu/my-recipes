import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { canActivate, redirectUnauthorizedTo } from "@angular/fire/compat/auth-guard";

const redirectUnauthorizedToAuth = () => redirectUnauthorizedTo(['auth']);


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth-routing/auth-routing.module').then(module => module.AuthRoutingModule)
  },
  {
    path: 'home',
    ...canActivate(redirectUnauthorizedToAuth),
    loadChildren: () => import('./home/home.module').then(module => module.HomeModule)
  },
  {
    path: 'recipe',
    ...canActivate(redirectUnauthorizedToAuth),
    loadChildren: () => import('./recipe/recipe.module').then(module => module.RecipeModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
