export enum FoodCategory {
  VEGETABLES,
  FRUITS,
  DAIRY_PRODUCTS,
  MEAT,
  SEAFOOD
}
