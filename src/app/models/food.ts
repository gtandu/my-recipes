import { FoodCategory } from "./food-category";
import { DatabaseObject } from "./databaseObject";

export class Food extends DatabaseObject {
  constructor(id: string, public name: string, public category: FoodCategory) {
    super(id);
  }
}
