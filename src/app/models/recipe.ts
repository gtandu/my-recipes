import { DatabaseObject } from "./databaseObject";
import { Ingredient } from "./ingredient";
import { Utensil } from "./utensil";
import { RecipeDetails } from "./recipe-details";
import { RecipeState } from "./recipe-state";

export class Recipe extends DatabaseObject {
  constructor(public name: string, public description: string, public creationDate: Date, id?: string, public recipeDetails?: RecipeDetails, public creator?: string | null, public ingredients?: Ingredient[], public utensils?: Utensil[], public recipeState?: RecipeState) {
    super(id);
  }
}
