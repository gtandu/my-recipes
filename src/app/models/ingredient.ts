import { DatabaseObject } from "./databaseObject";
import { Food } from "./food";

export class Ingredient extends DatabaseObject {
  constructor(id: string, public food: Food, public quantity: string) {
    super(id);
  }
}
