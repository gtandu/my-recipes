import { DatabaseObject } from "./databaseObject";

export class Utensil extends DatabaseObject{
  constructor(id: string, public name: string, public quantity: number) {
    super(id);
  }
}
