export enum Difficulty {
  EASY = 'EASY',
  MEDIUM = 'MEDIUM',
  EXPERT = 'EXPERT'
}
