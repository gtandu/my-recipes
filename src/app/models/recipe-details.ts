import { Difficulty } from "./difficulty";

export class RecipeDetails {
  constructor(public prepTime?: Date,public restTime?: Date, public cookTime?: Date, public serves?: number, public difficulty?: Difficulty) {
  }
}
