import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { SharedAngularFireModule } from "./shared/shared-angular-fire-module/shared-angular-fire.module";
import { AuthModule } from "./auth/auth.module";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { SharedModule } from "./shared/shared.module";
import { HomeModule } from "./home/home.module";
import { HeaderModule } from "./header/header.module";
import { AngularFireAuthGuard } from "@angular/fire/compat/auth-guard";
import { Lang } from "./models/lang";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SharedPrimeNgModule } from "./shared/shared-prime-ng/shared-prime-ng.module";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function translateFactory(translate: TranslateService) {
  return async () => {
    translate.setDefaultLang(Lang.FR);
    translate.use(Lang.FR);
    return new Promise<void>(resolve => {
      translate.onLangChange.subscribe(() => {
        resolve();
      });
    });
  };
}

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      extend: true
    }),
    AppRoutingModule,
    SharedAngularFireModule,
    SharedPrimeNgModule,
    SharedModule,
    AuthModule,
    HomeModule,
    HeaderModule
  ],
  providers: [AngularFireAuthGuard,
    {
      provide: APP_INITIALIZER,
      useFactory: translateFactory,
      deps: [TranslateService],
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
