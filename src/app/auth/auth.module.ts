import { NgModule } from '@angular/core';
import { LoginPageComponent } from "./login-page/login-page.component";
import { SignUpPageComponent } from "./sign-up-page/sign-up-page.component";
import { EmailComponent } from "./email/email.component";
import { AuthRoutingModule } from "./auth-routing/auth-routing.module";
import { SharedPrimeNgModule } from "../shared/shared-prime-ng/shared-prime-ng.module";
import { SharedModule } from "../shared/shared.module";


@NgModule({
  declarations: [
    LoginPageComponent,
    SignUpPageComponent,
    EmailComponent
  ],
  imports: [
    SharedPrimeNgModule,
    SharedModule,
    AuthRoutingModule
  ],
  exports: [
    LoginPageComponent,
    SignUpPageComponent,
    EmailComponent
  ]
})
export class AuthModule {
}
