import { Component, OnInit } from '@angular/core';
import { AuthService, LoginData } from "../../security/auth/auth.service";

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {

  public loginData: LoginData = new LoginData('', '');

  constructor(
    private authService: AuthService) {
  }

  ngOnInit() {
  }

  onSubmit(formData: { valid: any; value: { email: any; password: any; }; }) {
    if (formData.valid) {
      console.log(formData.value);
      this.authService.login({
          email: formData.value.email,
          password: formData.value.password
        }
      );
    }
  }

}
