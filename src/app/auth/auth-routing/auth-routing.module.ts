import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { LoginPageComponent } from "../login-page/login-page.component";
import { EmailComponent } from "../email/email.component";
import { SignUpPageComponent } from "../sign-up-page/sign-up-page.component";


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent },
  { path: 'email-login', component: EmailComponent },
  { path: 'signup', component: SignUpPageComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
