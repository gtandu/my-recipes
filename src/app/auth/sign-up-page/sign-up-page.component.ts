import { Component, OnInit } from '@angular/core';
import { AuthService, LoginData } from "../../security/auth/auth.service";

@Component({
  selector: 'app-sign-up-page',
  templateUrl: './sign-up-page.component.html',
  styleUrls: ['./sign-up-page.component.scss']
})
export class SignUpPageComponent implements OnInit {

  public loginData: LoginData = new LoginData('', '');

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

  onSubmit(formData: { valid: any; value: { email: any; password: any; }; }) {
    if (formData.valid) {
      this.authService.emailSignup(
        formData.value.email,
        formData.value.password
      );
    }
  }

}
