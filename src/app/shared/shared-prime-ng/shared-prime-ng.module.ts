import { NgModule } from '@angular/core';
import { ButtonModule } from "primeng/button";
import { CardModule } from "primeng/card";
import { SharedPrimeNgFormModule } from "./shared-prime-ng-form/shared-prime-ng-form.module";
import { ToastModule } from "primeng/toast";
import { MessageService } from "primeng/api";
import { OrderListModule } from "primeng/orderlist";
import { ChipModule } from "primeng/chip";


@NgModule({
  declarations: [],
  imports: [
    CardModule,
    ChipModule,
    ButtonModule,
    ToastModule,
    OrderListModule,
    SharedPrimeNgFormModule

  ], exports: [
    CardModule,
    ChipModule,
    ButtonModule,
    ToastModule,
    OrderListModule,
    SharedPrimeNgFormModule
  ],
  providers: [MessageService]
})
export class SharedPrimeNgModule {
}
