import { NgModule } from '@angular/core';
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { InputNumberModule } from "primeng/inputnumber";
import { PasswordModule } from "primeng/password";
import { AutoCompleteModule } from "primeng/autocomplete";
import { CalendarModule } from "primeng/calendar";
import { CheckboxModule } from "primeng/checkbox";
import { ChipsModule } from "primeng/chips";
import { DropdownModule } from "primeng/dropdown";
import { EditorModule } from "primeng/editor";
import { InputMaskModule } from "primeng/inputmask";
import { InputSwitchModule } from "primeng/inputswitch";
import { KeyFilterModule } from "primeng/keyfilter";
import { ListboxModule } from "primeng/listbox";
import { RadioButtonModule } from "primeng/radiobutton";
import { RatingModule } from "primeng/rating";


@NgModule({
  declarations: [],
  imports: [
    AutoCompleteModule,
    CalendarModule,
    CheckboxModule,
    ChipsModule,
    DropdownModule,
    EditorModule,
    InputMaskModule,
    InputTextModule,
    InputTextareaModule,
    InputSwitchModule,
    InputNumberModule,
    KeyFilterModule,
    ListboxModule,
    PasswordModule,
    RadioButtonModule,
    RatingModule,
  ],
  exports: [
    AutoCompleteModule,
    CalendarModule,
    CheckboxModule,
    ChipsModule,
    DropdownModule,
    EditorModule,
    InputMaskModule,
    InputTextModule,
    InputTextareaModule,
    InputSwitchModule,
    InputNumberModule,
    KeyFilterModule,
    ListboxModule,
    PasswordModule,
    RadioButtonModule,
    RatingModule
  ]
})
export class SharedPrimeNgFormModule {
}
