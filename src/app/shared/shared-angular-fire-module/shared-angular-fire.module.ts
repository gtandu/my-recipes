import { NgModule } from '@angular/core';
import { AngularFireModule } from "@angular/fire/compat";
import { environment } from "../../../environments/environment";
import { AngularFireAuthModule } from "@angular/fire/compat/auth";
import { AngularFireStorageModule } from "@angular/fire/compat/storage";


@NgModule({
  declarations: [],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFireStorageModule
  ],
  exports: [
    AngularFireModule,
    AngularFireAuthModule,
    AngularFireStorageModule
  ]
})
export class SharedAngularFireModule { }
