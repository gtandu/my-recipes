import { NgModule } from '@angular/core';
import { HeaderContainerComponent } from './header-container/header-container.component';
import { MenubarModule } from "primeng/menubar";
import { SlideMenuModule } from "primeng/slidemenu";
import { SharedPrimeNgModule } from "../shared/shared-prime-ng/shared-prime-ng.module";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [
    HeaderContainerComponent
  ],
  exports: [
    HeaderContainerComponent
  ],
  imports: [
    MenubarModule,
    SlideMenuModule,
    SharedModule,
    SharedPrimeNgModule
  ]
})
export class HeaderModule { }
