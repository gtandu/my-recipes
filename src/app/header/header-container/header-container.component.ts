import { Component, OnInit } from '@angular/core';
import { MenuItem } from "primeng/api";
import { AuthService } from "../../security/auth/auth.service";
import { TranslateService } from "@ngx-translate/core";
import { Lang } from "../../models/lang";
import { Router } from "@angular/router";

@Component({
  selector: 'app-header-container',
  templateUrl: './header-container.component.html',
  styleUrls: ['./header-container.component.scss']
})
export class HeaderContainerComponent implements OnInit {

  public menuItems: MenuItem[] = [];


  constructor(public authService: AuthService, private translateService: TranslateService, private router: Router) {
  }

  ngOnInit(): void {
    this.menuItems = this.getMenuItems();
  }

  routeToHome(){
    this.router.navigate(['/home'])
  }


  routeToRecipeForm(){
    this.router.navigate(['/recipe/create'])
  }

  checkCurrentLanguage(lang: string): string {
    return this.translateService.currentLang === lang ? 'fas fa-check' : '';
  }

  switchLang(lang: string): void {
    this.translateService.use(lang).subscribe(() => {
      this.menuItems = this.getMenuItems();
    });
  }

  signOut() {
    this.authService.logout();
  }

  private getMenuItems() {
    return [
      {
        label: this.translateService.instant("toolbar.language"), icon: 'fas fa-globe', items: [{
          label: this.translateService.instant("toolbar.language_fr"), icon: this.checkCurrentLanguage(Lang.FR), command: () => this.switchLang(Lang.FR)
        },
          {
            label: this.translateService.instant("toolbar.language_en"), icon: this.checkCurrentLanguage(Lang.EN), command: () => this.switchLang(Lang.EN)
          }]
      },
      {
        label: this.translateService.instant("toolbar.logout"), icon: 'fas fa-sign-out-alt', command: () => {
          this.authService.logout();
        },
      }
    ];
  }

}
