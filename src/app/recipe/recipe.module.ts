import { NgModule } from '@angular/core';
import { RecipeFormComponent } from './recipe-form/recipe-form.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedPrimeNgModule } from "../shared/shared-prime-ng/shared-prime-ng.module";
import { SharedModule } from "../shared/shared.module";
import { RecipeResolver } from "./recipe.resolver";
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';


const routes: Routes = [
  { path: 'create', component: RecipeFormComponent },
  { path: ':id', component: RecipeDetailsComponent, resolve: {
      recipe: RecipeResolver
    } }
];

@NgModule({
  declarations: [RecipeFormComponent, RecipeDetailsComponent],
  imports: [RouterModule.forChild(routes), SharedModule, SharedPrimeNgModule],
  exports: [RouterModule]
})
export class RecipeModule { }
