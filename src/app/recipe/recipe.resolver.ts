import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { catchError, EMPTY, mergeMap, Observable, of, take } from 'rxjs';
import { RecipeService } from "../services/recipe.service";

@Injectable({
  providedIn: 'root'
})
export class RecipeResolver implements Resolve<any> {

  constructor(public recipeService: RecipeService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.recipeService.getById(route.params['id']).pipe(
      take(1),
      mergeMap(something => {
        return something ? of(something) : EMPTY;
      }),
      catchError(() => {
        return EMPTY;
      }));
  }
}
