import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Recipe } from "../../models/recipe";

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.scss']
})
export class RecipeDetailsComponent implements OnInit {

  public recipe: Recipe | undefined;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.recipe = this.route.snapshot.data['recipe'];
  }

}
