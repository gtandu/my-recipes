import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Difficulty } from "../../models/difficulty";
import { TranslateService } from "@ngx-translate/core";
import { RecipeService } from "../../services/recipe.service";

@Component({
  selector: 'app-recipe-form',
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.scss']
})
export class RecipeFormComponent implements OnInit {

  public recipeForm: FormGroup;
  public difficulties: any[] = [{ name: null, key: null }];

  constructor(private fb: FormBuilder, private translateService: TranslateService, private recipeService: RecipeService) {
    this.recipeForm = this.fb.group({
      name: [null, [Validators.required]],
      description: [null, Validators.required],
      creationDate: [null, Validators.required],
      recipeDetails: this.fb.group({
        prepTime: [],
        cookTime: [],
        restTime: [],
        serves: [],
        difficulty: []
      }),
      ingredients: [],
      utensils: []
    });

    Object.keys(Difficulty).forEach((enumKey) => {
      let keyMessage = `recipe.form.recipeDetails.difficulty.${ enumKey.toLowerCase() }`;
      this.difficulties.push({ name: this.translateService.instant(keyMessage), key: enumKey })
    })
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.recipeForm.getRawValue()
    this.recipeService.createRecipe({...this.recipeForm.getRawValue()});
  }
}
