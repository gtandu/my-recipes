import { Injectable } from '@angular/core';
import { Recipe } from "../models/recipe";
import { from, Observable } from "rxjs";
import { AngularFirestore } from "@angular/fire/compat/firestore";
import { AngularFirestoreCollection } from "@angular/fire/compat/firestore/collection/collection";
import firebase from "firebase/compat";
import { AuthService } from "../security/auth/auth.service";
import DocumentReference = firebase.firestore.DocumentReference;
import { MessageService } from "primeng/api";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  private readonly recipesDbPath = 'recipes';

  public recipes: Observable<any[]> = new Observable<any[]>();

  public recipesCollection: AngularFirestoreCollection<Recipe>;

  constructor(private db: AngularFirestore, private auth: AuthService, private messageService: MessageService, private router: Router, private translateService: TranslateService) {
    this.recipesCollection = this.db.collection(this.recipesDbPath);
  }

  getAll(): Observable<Recipe[]>{
    return this.recipesCollection.valueChanges({ idField: 'id' });
  }

  getAllByUser(user: string | null | undefined): Observable<any[]>{
    return this.db.collection(this.recipesDbPath, ref => ref.where('creator','==', user).orderBy('creationDate')).valueChanges({ idField: 'id' });
  }

  getById(id: string): Observable<any>{
    return this.recipesCollection.doc(id).valueChanges();
  }

  createRecipe(recipe: Recipe): void {
    from(this.recipesCollection.add({...recipe, creationDate: new Date(), creator: this.auth.currentUserLogin})).subscribe({
      next: value => {
        this.messageService.add({severity:'success', summary: 'Succès', detail: this.translateService.instant('backend.create')});
        this.router.navigate(['']);
      },
        error: err => {
          this.messageService.add({severity:'error', summary: 'Erreur', detail: this.translateService.instant('backend.error')});
        },
        complete: () => console.log('complete')
    });
  }

  updateRecipe(recipe: Recipe) {
    return this.recipesCollection.doc(recipe.id).update(recipe);
  }

  deleteRecipeById(id: string): Observable<void>{
    return from(this.recipesCollection.doc(id).delete());
  }
}
