// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDAn-TBQAUzT-n1GGFd-MfrYK6vfVfWRys",
    authDomain: "my-recipes-ae8d5.firebaseapp.com",
    projectId: "my-recipes-ae8d5",
    storageBucket: "my-recipes-ae8d5.appspot.com",
    messagingSenderId: "683676530149",
    appId: "1:683676530149:web:1a0399012b24a3185aab0c",
    measurementId: "G-HMPYBB7MEK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

// Import the functions you need from the SDKs you need

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional


// Initialize Firebase
//const analytics = getAnalytics(app);
